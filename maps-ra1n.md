# Maps I Like
This is a list of maps I personally like, taken from [DevilDoc's
list](maps-doc.md) and
https://steamcommunity.com/workshop/filedetails/?id=1256729030.

<br /><br /><br />

## For HUGE Teamsizes
**school**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/642122953512041186/EF814060B1778817DAE204DF695D1F076597CA7B/" alt="Image of Map" height="250" />
 - An absolutely gorgeous map based around a school—but that's not nearly doing
   it justice. Lots of hidden tweaks in the map and a lot of detail!
 - https://steamcommunity.com/sharedfiles/filedetails/?id=560306383

**simpsons**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/964231063344297565/0DE27AAF3A1C3B5C7845B36666B3F9D3D42E7976/" alt="Image of Map" height="250" />
 - The Simpsons. Nuff said.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=556856618




<br /><br /><br />

## For LARGE Teamsizes

**6plusplus**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/608349439300169348/0AD9AC9F604B916EDA78CAB806A3B3F695575E58/" alt="Image of Map" height="250" />
 - A remake of `campgrounds`/`q3dm6` with no quad and a mega added at the
   central jump pad. The pillars area contains red armor and a passageway that
   extends to bridge. 
 - Gets played multiple times a day, so it can be a bit boring after awhile.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=587950516

**7plus**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/608349439300169552/18FC228C023696A76907DE88B1AB8E4515C838FB/" alt="Image of Map" height="250" />
 - A remake of `retribution` with a few more alleyways and the like.
 - Gets played most days, so it can be a bit boring after awhile.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=587950516

**chronic**  
<img src="https://lvlworld.com/levels/chronic/chroniclg.jpg" alt="Image of Map" height="250" />
 - A small section of a city.
 - https://lvlworld.com/overview/Chronic
 - https://lvlworld.com/review/Chronic
 - http://2002.eminem.pro/quake/index.html

**decidia**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/963104960326836400/0748F1691D6763163F3C8EF29AE85CA093CC992E/" alt="Image of Map" height="250" />
 - A large open foggy, swampy forest.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=1581095271

**ikzdm1**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/963104528641444353/981A14952315F433B101630CAAB0796E5FA84C52/" alt="Image of Map" height="250" />
 - "The Doom That Came To Dunwich"
 - Map of a small section of a city. Has an underground sewer complex with the
   quad, a battle suit hidden in the ceiling of a certain room, and red armor
   hidden in roof tops.
 - https://lvlworld.com/overview/id:793
 - https://lvlworld.com/review/id:793
 - https://ws.q3df.org/map/ikzdm1/
 - https://steamcommunity.com/sharedfiles/filedetails/?id=1578259628

**inversekill**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/269463394078603630/6601440B63B76B32A67FDF2EA260BA32E754DD15/" alt="Image of Map" height="250" />
 - `overkill` with a flipped/reversed layout.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=668999226

**ironwood**  
<img src="https://lvlworld.com/levels/ironwood/ironwood800x600.jpg" alt="Image of Map" height="250" />
 - "Ironwood"
 - Large map, obviously a CTF one tho
 - https://lvlworld.com/overview/id:2282
 - https://lvlworld.com/review/id:2282
 - https://steamcommunity.com/sharedfiles/filedetails/?id=842552687

**jof3dm1**   
<img src="https://lvlworld.com/levels/jof3dm1/jof3dm11280x960.jpg" alt="Image of Map" height="250" />
 - "The Long Way Home"
 - A nice map with a bull, secret rooms, and ancient feel to it.
 - https://lvlworld.com/overview/id:1610
 - https://lvlworld.com/review/id:1610
 - https://steamcommunity.com/sharedfiles/filedetails/?id=573112415

**pillcity**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/642123355803434920/F47E740806D725309C675C700ADAA48AFB36FA1F/" alt="Image of Map" height="250" />
 - An interesting city area.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=558637317

**q3gwdm1**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/955225420060944854/689A7728B9D5B7172B468CEB33C10C046D96CCB2/" alt="Image of Map" height="250" />
 - "Achromatic"
 - https://lvlworld.com/overview/Achromatic
 - https://lvlworld.com/review/Achromatic
 - https://steamcommunity.com/sharedfiles/filedetails/?id=1594168099
 
**q3gwdm2**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/955225420060907781/BE0AD3D51234FA948A7A011390357E46CFE4057E/" alt="Image of Map" height="250" />
 - "Endurance"
 - https://lvlworld.com/overview/id:2311
 - https://lvlworld.com/review/id:2311
 - https://steamcommunity.com/sharedfiles/filedetails/?id=1594165422
 
**wilderness**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/963104960326823469/A7FF1CB0079D1F1AE5548F754D41A1956426A595/" alt="Image of Map" height="250" />
 - Big castle. Multiple pits. Seems buggy ATM but a cool map.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=1581081443

**zastavka**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/642122953513504283/DCC2E478CF7A9426CFD3B545EAD1E9C07F5C0AF8/" alt="Image of Map" height="250" />
 - "Obec Zastávka u Brna"
 - A huge city map based on a Czech village.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=560255387





<br /><br /><br />

## For MEDIUM Teamsizes

**13base**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/853850080501011689/BF2E96B98B4957D80799914D47AD242CDD0EB1F0/" alt="Image of Map" height="250" />
 - "Hangar Base"
 - Big red-themed map with a pit in the center.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=1116819324

**charon3dm4**  
<img src="https://lvlworld.com/levels/map-charon3dm4/map-charon3dm4lg.jpg" alt="Image of Map" height="250" />
 - "Mu.innellim"
 - Dark grey map with interesting mirror-portal thingy and moving platforms
   around powerups.
 - https://lvlworld.com/overview/id:754
 - https://lvlworld.com/review/id:754
 - https://steamcommunity.com/sharedfiles/filedetails/?id=584176488

**cmp1dm6**  
<img src="https://lvlworld.com/levels/cmp1-dm6/cmp1-dm6lg.jpg" alt="Image of Map" height="250" />
 - "DSI:4.7"
 - White room. Reminds me a lot of the Star Wars Episode I: The Phantom Menace
   video game.
 - https://lvlworld.com/overview/id:1342
 - https://lvlworld.com/review/id:1342
 - https://steamcommunity.com/sharedfiles/filedetails/?id=583920201

**focalpoint**  
<img src="https://lvlworld.com/levels/focal_p132/focal_p132lg.jpg" alt="Image of Map" height="250" />
 - "Focal Point"
 - Cel-shaded map with interesting layout.
 - https://lvlworld.com/overview/Focal%20Point
 - https://lvlworld.com/review/Focal%20Point

**goldleaf**  
<img src="https://lvlworld.com/levels/goldleaf/goldleaf800x600.jpg" alt="Image of Map" height="250" />
 - "Goldleaf"
 - Interesting fast-paced map
 - https://lvlworld.com/overview/id:2307
 - https://lvlworld.com/review/id:2307
 - https://steamcommunity.com/sharedfiles/filedetails/?id=641510753

**hydra**  
<img src="https://lvlworld.com/levels/hydra/hydralg.jpg" alt="Image of Map" height="250" />
 - "Hydra"
 - Interesting industrial map for medium teams.
 - https://lvlworld.com/overview/id:2319
 - https://lvlworld.com/review/id:2319
 - https://steamcommunity.com/sharedfiles/filedetails/?id=850146040

**inbetween**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/856101548423110127/18EC28833BB20E241361BBE1AF662D0E14E1D42A/" alt="Image of Map" height="250" />
 - A remake of `houseofdecay` with a few less portals.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=1112471064

**inversekill**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/269463394078603630/6601440B63B76B32A67FDF2EA260BA32E754DD15/" alt="Image of Map" height="250" />
 - `overkill` with a flipped/reversed layout.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=668999226

**mksteel**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/500274055410945746/15C4E9B5D21A3A7ACC75BB8A7EF4FE7F05349856/" alt="Image of Map" height="250" />
 - "Fistful of Steel"
 - Map with blue silent carpets.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=676401107

**prot2**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/98348801210237514/82186BB1A9DB2DAC7EEBCB3E3A225F72BBF881BF/" alt="Image of Map" height="250" />
 - Remake of `provinggrounds` to be slightly bigger.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=821675354
 - https://ws.q3df.org/map/pro-q3tourney2/

**qfraggel1**  
<img src="https://lvlworld.com/levels/qfraggel1/qfraggel1lg.jpg" alt="Image of Map" height="250" />
 - "The Kristall Keep"
 - Map with lots of jump pads over pits.
 - https://lvlworld.com/overview/id:557
 - https://lvlworld.com/review/id:557
 - https://steamcommunity.com/sharedfiles/filedetails/?id=573807159

**qfraggel3tdm**  
<img src="https://ws.q3df.org/images/levelshots/512x384/qfraggel3tdm.jpg" alt="Image of Map" height="250" />
 - "Swiss Cheese Trickster"
 - Map with megahealth on weird green wall.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=573807159
 - https://ws.q3df.org/map/qfraggel3tdm/

**q3dmp23**   
<img src="https://steamuserimages-a.akamaihd.net/ugc/493519450185591905/9334A164A4624C9C2F010FB9F053377A883263D2/" alt="Image of Map" height="250" />
 - "Undergod"
 - Cool room with interesting glass bridge.
 - https://lvlworld.com/overview/Undergod
 - https://lvlworld.com/review/Undergod
 - https://steamcommunity.com/sharedfiles/filedetails/?id=679929214

**qxdm3**  
<img src="https://ws.q3df.org/images/levelshots/512x384/qxdm3.jpg" alt="Image of Map" height="250" />
 - "Station 52"
 - An interesting map with a cool red/grey/black color scheme.
 - https://lvlworld.com/overview/Station%2052
 - https://lvlworld.com/review/Station%2052
 - https://ws.q3df.org/map/qxdm3/
 
**polo3dm3**  
<img src="http://polo2ro.free.fr/shots/polo3dm3/big/dm3shot06.jpg" alt="Image of Map" height="250" />
 - "Dead Boy's Poem"
 - Red Room; big map in the style of `aerowalk`
 - http://polo2ro.free.fr/index.php3?shots=polo3dm3&out=shots/polo3dm3/big/dm3shot06.jpg

**polo3dm5**   
<img src="https://steamuserimages-a.akamaihd.net/ugc/395551414388251069/B72659B7C85754E98405C2A50FC6B5BC32C2CE3F/" alt="Image of Map" height="250" />
 - "Berserker"
 - A reddish map with interesting layout.
 - https://lvlworld.com/overview/Berserker
 - https://lvlworld.com/review/Berserker
 - https://steamcommunity.com/sharedfiles/filedetails/?id=562087739

**rota3dm3**  
<img src="https://lvlworld.com/levels/rota3dm3/rota3dm3lg.jpg" alt="Image of Map" height="250" />
 - "Marilyn"
 - Waterfall map with whale sounds.
 - https://lvlworld.com/overview/id:2010
 - https://lvlworld.com/review/id:2010
 - https://steamcommunity.com/sharedfiles/filedetails/?id=675614269 

**shibam**  
<img src="https://lvlworld.com/levels/shibam_v1.3/shibam_v1.3lg.jpg" alt="Image of Map" height="250" />
 - "Shibam"
 - A nice map modeled after the Yemeni town of [Shibam](https://en.wikipedia.org/wiki/Shibam).
 - https://lvlworld.com/overview/id:2148
 - https://lvlworld.com/review/id:2148
 - https://steamcommunity.com/sharedfiles/filedetails/?id=1587877622

**storm3tourney5**  
<img src="https://lvlworld.com/levels/storm3tourney5/storm3tourney5800x600.jpg" alt="Image of Map" height="250" />
 - "Cajun Hell"
 - Cool map with quad in a secret room with acid up high.
 - https://lvlworld.com/overview/id:1645
 - https://lvlworld.com/review/id:1645
 - https://steamcommunity.com/sharedfiles/filedetails/?id=564894881

**stormatordm1**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/963104960326816817/71AA9B631BF57D10D0BA11647F4E2D893A2967EE/" alt="Image of Map" height="250" />
 - "Stormatorium"
 - Caged mega map.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=1581073000

**zih_roof**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/395553315335209200/511E4ED8D3F9C1253E93E3D2287A4CEAD32709BC/" alt="Image of Map" height="250" />
 - "East Berlin Roofs"
 - Interesting map that shows the rooftops of East Berlin. Rail map, though.
 - https://lvlworld.com/overview/id:1972
 - https://lvlworld.com/review/id:1972
 - https://steamcommunity.com/sharedfiles/filedetails/?id=578409849





<br /><br /><br />

## For SMALL Teamsizes

**charon3dm5**  
<img src="https://lvlworld.com/levels/map-charon3dm5/map-charon3dm5lg.jpg" alt="Image of Map" height="250" />
 - "The Crucified Colony"
 - A map llama liked.
 - https://lvlworld.com/overview/id:841
 - https://lvlworld.com/review/id:841
 - https://steamcommunity.com/sharedfiles/filedetails/?id=584176488

**charon3dm7**  
<img src="https://lvlworld.com/levels/map-charon3dm7/map-charon3dm7lg.jpg" alt="Image of Map" height="250" />
 - "Mice are Blue"
 - One of Doc's maps. 
 - https://lvlworld.com/overview/id:872
 - https://lvlworld.com/review/id:872
 - https://steamcommunity.com/sharedfiles/filedetails/?id=584176488

**charon3dm13**  
<img src="https://en.ws.q3df.org/images/levelshots/512x384/charon3dm13.jpg" alt="Image of Map" height="250" />
 - "5quid"
 - Another charon3-series map. Played a lot with the UFT folks before.
 - https://lvlworld.com/overview/5quid
 - https://lvlworld.com/review/5quid
 - https://steamcommunity.com/sharedfiles/filedetails/?id=584176488

**ek3dm1**  
<img src="https://lvlworld.com/levels/ek3dm1/ek3dm1lg.jpg" alt="Image of Map" height="250" />
 - Another Doc map. Needs better lighting, but a decent map for small teams.
 - https://lvlworld.com/overview/The%20Abandoned%20Post
 - https://lvlworld.com/review/The%20Abandoned%20Post
 - https://steamcommunity.com/sharedfiles/filedetails/?id=583821136 

**jex3dm1**  
<img src="https://lvlworld.com/levels/jex3dm1/jex3dm1800x600.jpg" alt="Image of Map" height="250" />
 - "Eulogy"
 - An interesting industrial map that requires a fair amount of strategy to do well in..
 - https://lvlworld.com/overview/eulogy
 - https://lvlworld.com/review/eulogy
 - https://steamcommunity.com/sharedfiles/filedetails/?id=583820600

**lun3dm4**  
<img src="https://lvlworld.com/levels/lun3dm4/lun3dm4lg.jpg" alt="Image of Map" height="250" />
 - Small, fast-paced map with lots of interesting portals and jump pads.
 - https://lvlworld.com/overview/id:1562
 - https://lvlworld.com/review/id:1562
 - https://steamcommunity.com/sharedfiles/filedetails/?id=948354582G

**ospdm3**  
<img src="http://en.ws.q3df.org/images/levelshots/512x384/ospdm3.jpg" alt="Image of Map" height="250" />  
 - "Scrap Metal ]["
 - A small blue map  
 - "Rain Room"  
 - http://en.ws.q3df.org/map/ospdm3/   

**oxodm2a**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/388795474460971625/F7BAC36EC68256E3A629567B53A725A26D7539F8/" alt="Image of Map" height="250" />
 - `aerowalk` meets `bloodrun`
 - https://steamcommunity.com/sharedfiles/filedetails/?id=560548198

**Quake3Stuff**  
<img src="https://lvlworld.com/levels/quake3stuff/quake3stufflg.jpg" alt="Image of Map" height="250" />
 - A nice icy map.
 - https://lvlworld.com/overview/Quake3Stuff
 - https://lvlworld.com/review/Quake3Stuff
 - https://steamcommunity.com/sharedfiles/filedetails/?id=567348670

**quakemonkeys**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/778406850974316287/645F5E6366EADBF60053C01FEFBFEB1880F1ABE3/" alt="Image of Map" height="250" />
 - A map that looks like a garden with a very-hard-to-get quad.
 - https://lvlworld.com/overview/Quake%20Monkeys
 - https://lvlworld.com/review/Quake%20Monkeys
 - https://steamcommunity.com/sharedfiles/filedetails/?id=934170954
 
**reise**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/90470964771208735/17115AD79E113B47D04C949F7C241BCC373BBE36/" alt="Image of Map" height="250" />
 - Orange/White cel-shaded map. I like it a lot, but it hurts your eyes.
 - https://lvlworld.com/overview/id:1908
 - https://lvlworld.com/review/id:1908
 - https://steamcommunity.com/sharedfiles/filedetails/?id=828614694

**tortured**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/98349353834864356/84A216EB207C3B1F1C0751E8598E883C16D83F7B/" alt="Image of Map" height="250" />
 - Steampunk-esque map. Pretty fun.
 - https://steamcommunity.com/sharedfiles/filedetails/?id=823411116
 - https://lvlworld.com/overview/Tortured
 - https://lvlworld.com/review/Tortured

**trespass**  
<img src="https://steamuserimages-a.akamaihd.net/ugc/90470964770198816/EFD97CDED3CF52B2E5DD56992F818FB33C2F0141/" alt="Image of Map" height="250" />
 - "Trespass"
 - Very cool map that goes by quick.
 - https://lvlworld.com/overview/id:2323
 - https://lvlworld.com/review/id:2323
 - https://steamcommunity.com/sharedfiles/filedetails/?id=838471086

